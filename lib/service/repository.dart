

import 'package:app/constants/api_url.dart';
import 'package:app/modul/login/model/login_model.dart';
import 'package:app/service/api_service.dart';

class Repository {
  final _apiService = ApiServices(baseDevUrl);

  Future<LoginModel> Login(data) async{
    final response = await _apiService.call(loginUrl, request: data);
    if(response?.statusCode == 200){
      print("aoke ini");
      return LoginModel.fromJson(response?.data);
    }else{
      return LoginModel(code: response?.statusCode, info:"error");
    }
  }
  
}