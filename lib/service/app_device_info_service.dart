import 'dart:io';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:package_info_plus/package_info_plus.dart';
// import 'package:safe_device/safe_device.dart';

class AppDeviceInfoService {
  late PackageInfo _packageInfo;

  late AndroidDeviceInfo _androidDeviceInfo;

  late IosDeviceInfo _iosDeviceInfo;

  bool? isDeviceRooted;

  String? appName;

  String? packageName;

  String? version;

  String? buildNumber;

  String get deviceUniqueId => _deviceUniqueId();

  String _deviceUniqueId() {
    if (Platform.isAndroid) {
      // return '${_androidDeviceInfo.device} (${_androidDeviceInfo.id})';
      return '${_androidDeviceInfo.id}';
    } else {
      // return '${_iosDeviceInfo.name} (${_iosDeviceInfo.model})';
      return '${_iosDeviceInfo.identifierForVendor}';
    }
  }

  Future setup() async {
  //  isDeviceRooted = await RootChecker.isDeviceRooted;
    // isDeviceRooted = await SafeDevice.isJailBroken;
    // print("status root devices ${isDeviceRooted.toString()}");
    if (Platform.isAndroid)
      _androidDeviceInfo = await DeviceInfoPlugin().androidInfo;
    if (Platform.isIOS) _iosDeviceInfo = await DeviceInfoPlugin().iosInfo;
    _packageInfo = await PackageInfo.fromPlatform();
    appName = _packageInfo.appName;
    packageName = _packageInfo.packageName;
    version = _packageInfo.version;
    buildNumber = _packageInfo.buildNumber;
  }
}