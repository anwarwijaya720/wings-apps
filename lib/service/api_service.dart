import 'package:dio/dio.dart';
import 'dart:developer' as printDebug;

enum MethodRequest { POST, GET, PUT, DELETE }

class ApiServices {

  static Dio _dio = Dio();

  ApiServices(String baseUrl) {
    _dio.options.baseUrl = baseUrl;
    _dio.options.connectTimeout = 90000; //90s
    _dio.options.receiveTimeout = 10000;
    _dio.options.headers = {'Accept': 'application/json'};
  }
  
  Future<Response?> call(
    String url, 
    {
      MethodRequest method = MethodRequest.POST, 
      Map<String, dynamic>? request, 
      Map<String, String>? header, 
      String? token,
      bool useFormData = false
    }
    ) async {
      if(header != null){
      _dio.options.headers = header;
    }
    if(token != null){
      if(header != null){
        header.addAll({
          'Authorization': '$token'
        });
        _dio.options.headers = header;
      }else{
        _dio.options.headers = {
          'Accept': 'application/json',
          'Authorization': '$token'
        };
      }
      if(method == MethodRequest.PUT){
        _dio.options.headers = {
          'Accept': 'application/json',
          'Authorization': '$token',
          'Content-Type': 'application/x-www-form-urlencoded'
        };
      }
    }

    print('URL : ${_dio.options.baseUrl}$url');
    print('Method : $method');
    print('Header : ${_dio.options.headers}');
    print('Request : $request');
    var selectedMethod;

    try{
      Response response;
      switch (method) {
        case MethodRequest.GET:
          selectedMethod = method;
          response = await _dio.get(url, queryParameters: request, );
          break;
        case MethodRequest.PUT:
          selectedMethod = method;
          response = await _dio.put(url,
              data: request,
          );
          break;
        case MethodRequest.DELETE:
          selectedMethod = method;
          response = await _dio.delete(
            url,
            data: useFormData ? FormData.fromMap(request!) : request,
          );
          break;
        default:
          selectedMethod = MethodRequest.POST;
          response = await _dio.post(
            url,
            data: useFormData ? FormData.fromMap(request!) : request,
          );
      }

      print(response.data);
      print("oke ini");

      if (response.data is Map) {
        print('Success $selectedMethod $url: \nResponse : ${response.data}');
        return response;
      } else {
        print('Success NOT MAP $selectedMethod $url: \nResponse : ${response.data}');
        var error = Response(statusCode: 400, data: {
          'error': 'Terjadi kesalahan, coba lagi nanti',
          'result': false,
        }, requestOptions: RequestOptions(
          path: ''
        ));
        return error;
      }
    } on DioError catch (e){
      print('Error $selectedMethod $url: $e\nData: ${e.response?.data}');
      return e.response;
    }
  }

}