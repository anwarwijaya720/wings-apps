import 'package:encrypt/encrypt.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LocalStorageService {
  static LocalStorageService? _instance;
  static SharedPreferences? _preferences;

  static final _key = Key.fromUtf8('wingsApp@12345');
  static final _iv = IV.fromLength(16);

  static final _encrypter = Encrypter(AES(_key));

  static const String IntroKey = 'intro';
  static const String FcmKey = 'fcm';
  static const String LangKey = 'languange';
  static const String TokenKey = 'token';
  static const String RfidKey = 'rfid';
  static const String NameKey = 'name';
  static const String NisKey = 'nis';
  static const String KelasKey = 'kelas';
  static const String FotoKey = 'foto';

  bool get intro => _getFromDisk(IntroKey) ?? false;
  set intro(bool value) => _saveToDisk(IntroKey, value);

  String? get fcm => _getFromDisk(FcmKey);
  set fcm(String? value) => _saveToDisk(FcmKey, value);

  String? get token => _getFromDisk(TokenKey);
  set token(String? value) => _saveToDisk(TokenKey, value);

  String? get languange => _getFromDisk(LangKey);
  set languange(String? value) => _saveToDisk(LangKey, value);

  String? get name => _getFromDisk(NameKey);
  set name(String? value) => _saveToDisk(NameKey, value);

  String? get foto => _getFromDisk(FotoKey);
  set foto(String? value) => _saveToDisk(FotoKey, value);

  static Future<LocalStorageService?> getInstance() async {
    if (_instance == null) {
      _instance = LocalStorageService();
    }
    if (_preferences == null) {
      _preferences = await SharedPreferences.getInstance();
    }
    return _instance;
  }

  void setLoginPref(String? name, String token) {    
    this.name = name;
    this.token = token;
  }

  Future<void> setLogoutPref() async{
    _preferences!.remove("token");
    _preferences!.remove("name");
  }

  /// Get dynamic value from all type preferences
  dynamic _getFromDisk(String key, {bool decrypt = false}) {
    var value = _preferences!.get(key);
    if (decrypt && value != null)

    /// old version of encrypt 2.0
    ///value = _encrypter.decrypt64(value);
    ///notes: starting from encyrpt latest version (4.0.2) using below code
    value = _encrypter.decrypt64(value as String, iv: _iv);
    print('(TRACE) LocalStorageService:_getFromDisk. key: $key value: $value instansce of: ${value.runtimeType}');
    if (value is List) return value.cast<String>();
    return value;
  }

  /// Save data function that handles all types
  void _saveToDisk<T>(String key, T content, {bool encrypt = false}) {
    print('(TRACE) LocalStorageService:_saveToDisk. key: $key value: $content');
    if (content == null) {
      _preferences!.setString(key, '');
    }
    if (content is String) {
      if (encrypt) {
        /// old version of encrypt 2.0
        ///var contentEncrypted = _encrypter.encrypt(content);
        ///notes: starting from encyrpt latest version (4.0.2) using below code
        var contentEncrypted = _encrypter.encrypt(content, iv: _iv);
        _preferences!.setString(key, contentEncrypted.base64);
      } else {
        _preferences!.setString(key, content);
      }
    }
    if (content is bool) {
      _preferences!.setBool(key, content);
    }
    if (content is int) {
      _preferences!.setInt(key, content);
    }
    if (content is double) {
      _preferences!.setDouble(key, content);
    }
    if (content is List<String>) {
      _preferences!.setStringList(key, content);
    }
  }

}