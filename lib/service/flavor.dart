import 'package:app/constants/api_url.dart';

enum Flavor {DEVELOPMENT, PRODUCTION, UAT}

class FlavorConfig {
  static Flavor? flavor;

  static String baseUrl() {
    assert(flavor != null, "Need initiated flavor type first");
    switch (flavor) {
      case Flavor.PRODUCTION:
        return basePodUrl;
        break;
      case Flavor.UAT:
        return baseDevUrl;
        break;
      default:
        return baseDevUrl;
    }
  }

}
