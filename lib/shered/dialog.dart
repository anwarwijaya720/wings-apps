import 'package:flutter/material.dart';

void showSnackBar(BuildContext context, msg){
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      content: Text('${msg.toString()}'),
    )
  );
}