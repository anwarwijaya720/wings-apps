import 'dart:async';

import 'package:app/shered/regex.dart';

class Validators {

  final performCheckEmptyForm = StreamTransformer<String, String>.fromHandlers(
    handleData: (input,sink){
      if(input.isNotEmpty){    
        sink.add(input);
      }else{
        sink.addError("kolom wajib diisi");    
      }
    }
  );

  final validationNis = StreamTransformer<String, String>.fromHandlers(
    handleData: (username,sink){
      if(username.isEmpty){
        sink.addError('Lengkapi data ini');
      }else{
        if(username.length <= 6){
          sink.addError("NIS must be at least 3 characters");        
        }else {
          sink.add(username);
        }
      }
    }
  );

  final validationTextFrom = StreamTransformer<String, String>.fromHandlers(
    handleData: (data,sink){
      if(data.isEmpty){
        sink.addError('Lengkapi data ini');
      }else{
          sink.add(data);
      }
    }
  );

  final validationUsername = StreamTransformer<String, String>.fromHandlers(
    handleData: (username,sink){
      if(username.isEmpty){
        sink.addError('Lengkapi data ini');
      }else{
        if(username.length <= 2){
          sink.addError("Username must be at least 3 characters");        
        }else {
          sink.add(username);
        }
      }
    }
  );

  final performCellularNoValidation =
    StreamTransformer<String, String>.fromHandlers(
      handleData: (accountNo, sink) {
        if(accountNo.isEmpty){
          sink.addError('Lengkapi data ini');
        }else{
          if (accountNo.startsWith("8")) {
            RegExp regExp = new RegExp(numberValidationRule);
            if(accountNo.length < 9){
              sink.addError('Nomor handphone minimal 9 karakter');
            }else if(accountNo.length > 16){
              sink.addError('Nomor handphone maksimal 16 karakter');
            }else if (regExp.hasMatch(accountNo)) {
              sink.add(accountNo);
              // sink.add("+62" + accountNo);
              // sink.add("0" + accountNo);
            }else {
              sink.addError('Format Nomor Telepon salah');
            }
          }else{
            sink.addError('Format Nomor Telepon salah');
          }
        }
  });

  final performPinValidation = StreamTransformer<String, String>.fromHandlers(
    handleData: (otp, sink) {
      RegExp otpRule = new RegExp(numberValidationRule);
      if (otpRule.hasMatch(otp)) {
        if (otp.isEmpty) {
          sink.addError('Lengkapi data ini');
        }else{
          if (otp.length == 6) {
            sink.add(otp);
          } else {
            // sink.addError('Masukan 6 digit OTP');
            sink.addError('Lengkapi data ini');
          }
        }
      }else {
        sink.addError('Hanya dapat diisi dengan angka');
      }
  });

}