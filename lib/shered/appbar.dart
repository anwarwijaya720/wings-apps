import 'package:app/shered/colors.dart';
import 'package:app/shered/text_style.dart';
import 'package:flutter/material.dart';


appBarHome(titleBar){
  return AppBar(
    elevation: 0,
    automaticallyImplyLeading: false,
    title: Text("${titleBar.toString()}",
      style: gooleFontTextSylePoppins(Colors.white, "24", FontWeight.w600),
    ),
    centerTitle: true,
    backgroundColor: bgRed,
  );
}

appBackBar(titleBar){
  return AppBar(
    elevation: 0,
    title: Text("${titleBar.toString()}",
      style: gooleFontTextSylePoppins(Colors.white, "20", FontWeight.w600),
    ),
    centerTitle: true,
    backgroundColor: bgRed,
  );
}

appBackBarSearch(BuildContext context){
  return AppBar(
    elevation: 0,
    centerTitle: true,
    automaticallyImplyLeading: false, 
    backgroundColor: Colors.red,
    flexibleSpace: Padding(
      padding: const EdgeInsets.only(top: 60, left: 20, right: 20),
      child: Row(
        children: [
          InkWell(
            onTap: (){
              Navigator.of(context).pop();
            },
            child: Icon(Icons.arrow_back_outlined, color: Colors.white, size: 30)
          ),
          SizedBox(width: 10),
          Expanded(
            child: Container(
              width: 46,
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(10))
              ),
              child: Row(
                children: [
                  Icon(Icons.search, color: Colors.grey),
                  SizedBox(width: 10),
                  Text("Cari Produk", 
                    style: infoTextSyle(Colors.grey, "14", FontWeight.normal),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                  )
                ],
              ),
            ),
          )
        ],
      ),
    ),
  );
}



