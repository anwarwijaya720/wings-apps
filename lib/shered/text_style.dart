import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

TextStyle gooleFontTextSylePoppins(_color, _fontSize, _fontWeight){
  return GoogleFonts.poppins().copyWith(
    color: _color,
    fontSize: double.parse(_fontSize),
    fontWeight: _fontWeight,
    letterSpacing: 0.8
  );
}

TextStyle gooleFontTextSyleYanone(_color, _fontSize, _fontWeight){
  return GoogleFonts.yanoneKaffeesatzTextTheme().headline6!.copyWith(
    color: _color,
    fontSize: double.parse(_fontSize),
    fontWeight: _fontWeight,
    letterSpacing: 0.8,
  );
}

TextStyle infoTextSyle(_color, _fontSize, _fontWeight){
  return TextStyle(
    color: _color,
    fontSize: double.parse(_fontSize),
    fontWeight: _fontWeight
  );
}

TextStyle bodyTextStyle = GoogleFonts.openSans(
  color: Colors.black,
);