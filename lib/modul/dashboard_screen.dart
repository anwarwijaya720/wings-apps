import 'package:app/modul/home/screen/home.dart';
import 'package:app/modul/profile/screen/profile_screen.dart';
import 'package:app/modul/promo/screen/promo_screen.dart';
import 'package:app/shered/colors.dart';
import 'package:flutter/material.dart';

class DashboardScreen extends StatefulWidget {
  const DashboardScreen({ Key? key }) : super(key: key);

  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {

  int selectedIndex = 0;
  DateTime? currentBackPressTime;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void onItemTapped(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  Future<bool> onWillPop() {
    DateTime now = DateTime.now();
    if (currentBackPressTime == null || now.difference(currentBackPressTime!) > Duration(seconds: 2)) {
      showDialog<bool>(
        context: context,
        builder: (c) => AlertDialog(
          title: Text('Warning'),
          content: Text('Do you really want to exit'),
          actions: [
            FlatButton(
              child: Text('Yes'),
              onPressed: () => Navigator.pop(c, true),
            ),
            FlatButton(
              child: Text('No'),
              onPressed: () => Navigator.pop(c, false),
            ),
          ],
        ),
      );
    }
    return Future.value(true);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: (){
        return onWillPop();
      },
      child: Scaffold(
        extendBody: true,
        body: mainMenu(),
        bottomNavigationBar: Theme(
          data: Theme.of(context).copyWith(
            // canvasColor: Colors.blueGrey,
          ),
          child: btnNavBar(),
        ),
      ),
    );
  }

  Widget btnNavBar() {
    return BottomNavigationBar(
      items: const <BottomNavigationBarItem>[        
        BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
        BottomNavigationBarItem(icon: Icon(Icons.library_books), label: 'Promo'),
        BottomNavigationBarItem(icon: Icon(Icons.person), label: 'Profile'),
      ],  
      currentIndex: selectedIndex,
      onTap: onItemTapped,
      selectedItemColor: bgRed,
      unselectedItemColor: Colors.grey,
      showUnselectedLabels: true,
    );
  }

  Widget mainMenu() {
    var children2 = <Widget>[
        HomeScreen(),
        PromoScreen(),
        ProfileScreen()
      ];
    return IndexedStack(
      index: selectedIndex,
      children: children2,
    );
  }

}