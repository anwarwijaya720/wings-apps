import 'package:app/shered/appbar.dart';
import 'package:app/shered/text_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class ProductDetail extends StatefulWidget {
  const ProductDetail({Key? key}) : super(key: key);

  @override
  State<ProductDetail> createState() => _ProductDetailState();
}

class _ProductDetailState extends State<ProductDetail> {

  late Size size;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {    
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: appBackBar("Product Detail"),
      body: Container(
        width: size.width,
        alignment: Alignment.center,
        padding: EdgeInsets.only(top: 50, left: 20, right: 20),
        child: Column(
          children: [
            Column(
              children: [
                Container(
                  width: 150,
                  height: 150,
                  decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.all(Radius.circular(50))
                  ),
                ),
                SizedBox(height: 18),
                Text("Prodak Name", style: gooleFontTextSylePoppins(Colors.black, "24", FontWeight.bold),)
              ],
            ),
            SizedBox(height: 40),
            Container(
              width: size.width,
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Price", style: gooleFontTextSylePoppins(Colors.black, "18", FontWeight.normal)),
                      Text("Rp 18.000", style: gooleFontTextSylePoppins(Colors.black, "18", FontWeight.normal)),
                    ],
                  ),
                  SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Dimension", style: gooleFontTextSylePoppins(Colors.black, "18", FontWeight.normal)),
                      Text("13 cm x 10 cm", style: gooleFontTextSylePoppins(Colors.black, "18", FontWeight.normal)),
                    ],
                  ),
                  SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Price Unit", style: gooleFontTextSylePoppins(Colors.black, "18", FontWeight.normal)),
                      Text("Pcs", style: gooleFontTextSylePoppins(Colors.black, "18", FontWeight.normal)),
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}