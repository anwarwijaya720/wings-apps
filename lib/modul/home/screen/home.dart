import 'package:app/modul/home/screen/product_detail.dart';
import 'package:app/shered/appbar.dart';
import 'package:app/shered/text_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  @override
  void initState() {    
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarHome("Product"),
      body: ListView.builder(
        itemCount: 10,
        itemBuilder: (context, index) {
          return Container(
            padding: EdgeInsets.all(18),
            child: Row(
              children: [
                Container(
                  width: 80,
                  height: 80,
                  decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.all(Radius.circular(50))
                  ),
                ),
                SizedBox(width: 30),
                Expanded(
                  child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Prodak Name", style: gooleFontTextSylePoppins(Colors.black, "15", FontWeight.bold)),
                      SizedBox(height: 10),
                      Text("Rp 20.000",
                        style: bodyTextStyle.copyWith(
                          fontSize: 14,
                          height: 0.9,
                          fontWeight: FontWeight.w500,
                          color: Colors.black,
                          decoration:TextDecoration.lineThrough,
                          decorationColor: Colors.grey,
                          decorationThickness: 2,
                        ),
                      ),
                      SizedBox(height: 10),
                      Text("Rp 15.000"),
                      SizedBox(height: 10),
                      Container(                        
                        child: ElevatedButton(
                          child: Text('BUY',
                            style: gooleFontTextSylePoppins(Colors.white, "18", FontWeight.w500),
                            overflow: TextOverflow.ellipsis,
                          ),
                          style: ElevatedButton.styleFrom(
                            primary: Colors.blueAccent,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                          onPressed: (){
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => const ProductDetail()),
                              );
                            },
                          ),
                        )
                      ],
                    ),
                  )
                )
              ],
            ),
          );
        },
      ),
    );
  }
}