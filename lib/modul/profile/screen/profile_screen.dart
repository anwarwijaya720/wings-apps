import 'package:app/modul/login/screen/login_screen.dart';
import 'package:app/shered/appbar.dart';
import 'package:flutter/material.dart';

import 'package:app/shered/appbar.dart';
import 'package:app/shered/text_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {

  late Size size;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {    
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: appBarHome("Product Detail"),
      body: Container(
        width: size.width,
        alignment: Alignment.center,
        padding: EdgeInsets.only(top: 50, left: 20, right: 20),
        child: Column(
          children: [
            Column(
              children: [
                Container(
                  width: 150,
                  height: 150,
                  decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.all(Radius.circular(50))
                  ),
                ),
                SizedBox(height: 18),
                Text("Username", style: gooleFontTextSylePoppins(Colors.black, "24", FontWeight.bold),)
              ],
            ),
            SizedBox(height: 40),            
            InkWell(
              onTap: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const LoginScreen()),
                );
              },
              child: Card(
                elevation: 5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10))
                ),
                child: Center(
                  child: Container(
                    padding: EdgeInsets.only(left: 10),
                    height: 50,
                    child: Row(
                      children: [
                        Icon(Icons.exit_to_app),
                        SizedBox(width: 10),
                        Text("logout", style: gooleFontTextSylePoppins(Colors.black, "14", FontWeight.w500))
                      ],
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}