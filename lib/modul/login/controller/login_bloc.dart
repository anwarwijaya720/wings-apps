import 'package:app/helper/base_bloc.dart';
import 'package:app/modul/dashboard_screen.dart';
import 'package:app/service/local_storage_service.dart';
import 'package:app/service/repository.dart';
import 'package:app/shered/dialog.dart';
import 'package:app/shered/validator.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class LoginBloc extends BaseBloc with Validators {
  
  var repo = Repository();
  var userController = TextEditingController();
  var passController = TextEditingController();

  final _user = BehaviorSubject<String>();
  final _pass = BehaviorSubject<String>();

  //Set
  Function(String) get setUser => _user.sink.add;
  Function(String) get setPass => _pass.sink.add;

  //Get
  Stream<String> get user => _user.stream.transform(performCheckEmptyForm);
  Stream<String> get pass => _pass.stream.transform(performCheckEmptyForm);

  Stream<bool> get addAddressValid => Rx.combineLatest2(user, pass,(dynamic a, dynamic b) => true);

  validateForm(){
    if (userController.text.isEmpty) {
      _user.sink.addError("kolom wajib diisi");
    }
    if (passController.text.isEmpty) {
      _pass.sink.addError("kolom wajib diisi");
    }
  }

  Future actionLogin(BuildContext context) async {
    setBusy();
    final localStorage = await LocalStorageService.getInstance();
    String? fcmToken = localStorage?.fcm.toString();
    var data = {
      "user" : "${_user.value.toString()}",
	    "pass" : _pass.value,
      // "fcm_token" : "${fcmToken}"
    };

    var loginRespon = await repo.Login(data);

    if(loginRespon.code == 200){
      String token = "Bearer ${loginRespon.token}";
      String? name = loginRespon.data?[0].username;
      localStorage?.setLoginPref(name.toString(), token);
      Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (BuildContext context) => DashboardScreen()),
        (Route<dynamic> route) => route.isFirst,
      );
    }else{
      showSnackBar(context, "${loginRespon.info.toString()}");
    }
    setIdle();
  }
  
}