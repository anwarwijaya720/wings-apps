class LoginModel {
  int? code;
  String? info;
  String? token;
  List<Data>? data;

  LoginModel({this.code, this.info, this.token, this.data});

  LoginModel.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    info = json['info'];
    token = json['token'];
    if (json['data'] != null) {
      data = <Data>[];
      json['data'].forEach((v) {
        data!.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['info'] = this.info;
    data['token'] = this.token;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int? no;
  String? userId;
  String? username;
  String? password;
  String? createDate;
  int? status;

  Data(
      {this.no,
      this.userId,
      this.username,
      this.password,
      this.createDate,
      this.status});

  Data.fromJson(Map<String, dynamic> json) {
    no = json['no'];
    userId = json['user_id'];
    username = json['username'];
    password = json['password'];
    createDate = json['create_date'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['no'] = this.no;
    data['user_id'] = this.userId;
    data['username'] = this.username;
    data['password'] = this.password;
    data['create_date'] = this.createDate;
    data['status'] = this.status;
    return data;
  }
}
