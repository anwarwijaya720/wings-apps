import 'package:app/modul/dashboard_screen.dart';
import 'package:app/modul/login/controller/login_bloc.dart';
import 'package:app/shered/colors.dart';
import 'package:app/shered/text_style.dart';
import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({ Key? key }) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {

  var bloc = LoginBloc();
  bool passwordVisible = true;
  late Size size;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: 80),
              Center(
                child: Text(
                  "WINGS GROUP",
                  style: gooleFontTextSylePoppins(kBlackColor, "24", FontWeight.w600),
                ),
              ),
              SizedBox(height: 70),
              txvUsername(),
              SizedBox(height: 15),
              txvPassword(),
              // txtBoxPinCode(),
              SizedBox(height: 20),
              btnSubmit()
            ],
          ),
        ),
      ),
    );
  }

  Widget txvUsername(){
    return StreamBuilder(
      stream: bloc.user,
      builder: (context, snapshot){
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Input Username"),
            SizedBox(height: 10),
            TextFormField(
              controller: bloc.userController,
              onChanged: bloc.setUser,
              keyboardType: TextInputType.multiline,
              style: const TextStyle(
                  color: Colors.black,
              ),
              decoration: new InputDecoration(
                filled: true,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                  borderSide: BorderSide(
                    color: Colors.blue,
                    width: 2,
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                  borderSide: BorderSide(
                    color: Colors.blue,
                    width: 2,
                  ),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                  borderSide: BorderSide(
                    color: Colors.grey,
                    width: 2,
                  ),
                ),
                // hintText: "Input Nomor Induk Siswa",
                hintStyle: const TextStyle(color: Colors.black, fontSize: 14.0),
                errorText: snapshot.error?.toString(),
                errorStyle: TextStyle(color: Colors.red),
              ),
            ),
          ],
        );
      }
    );
  }

  Widget txvPassword(){
    return StreamBuilder(
      stream: bloc.pass,
      builder: (context, snapshot){
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Input Password"),
            SizedBox(height: 10),
            TextFormField(
              controller: bloc.passController,
              onChanged: bloc.setPass,
              obscureText: true,
              enableSuggestions: false,
              autocorrect: false,
              style: const TextStyle(
                  color: Colors.black,
              ),
              decoration: new InputDecoration(
                filled: true,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                  borderSide: BorderSide(
                    color: Colors.blue,
                    width: 2,
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                  borderSide: BorderSide(
                    color: Colors.blue,
                    width: 2,
                  ),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                  borderSide: BorderSide(
                    color: Colors.grey,
                    width: 2,
                  ),
                ),
                // hintText: "Input Nomor Induk Siswa",
                hintStyle: const TextStyle(color: Colors.black, fontSize: 14.0),
                errorText: snapshot.error?.toString(),
                errorStyle: TextStyle(color: Colors.red),
              ),
            ),
          ],
        );
      }
    );
  }

  Widget btnSubmit(){
    return StreamBuilder<bool>(
      stream: bloc.addAddressValid,
      builder: (context, snapshot) {
        return Container(
          width: size.width,
          height: 45,
          child: ElevatedButton(
            child: Text('Login',
              style: gooleFontTextSylePoppins(Colors.white, "18", FontWeight.w500),
              overflow: TextOverflow.ellipsis,
            ),
            style: ElevatedButton.styleFrom(
              primary: Colors.blueAccent,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
            onPressed: (){
              snapshot.hasData == true ? 
                // bloc.actionLogin(context) 
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const DashboardScreen()),
                )
              : bloc.validateForm();              
            },
          ),
        );
      }
    );
  }

}