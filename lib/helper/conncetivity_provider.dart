import 'dart:async';
import 'dart:io';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';

class ConnectivityProvider with ChangeNotifier {
  
  final Connectivity _connectivity = Connectivity();

  bool? _isOnline;
  bool? get isOnline => _isOnline;

  startMonitoring() async{
    await initConncetivity();
    _connectivity.onConnectivityChanged.listen((result) async {
      if(result == ConnectivityResult.none){
        _isOnline = false;
        print("none status ${_isOnline.toString()}");
        notifyListeners();
      }else{
        await _updateConncetionStatus().then((bool isConnceted) => {
          _isOnline = isConnceted,
          print("status ${_isOnline.toString()}"),
          notifyListeners(),
        });
      }
    });
  }

  Future<void> initConncetivity() async{
    try {
      var status = await _connectivity.checkConnectivity();
      if(status == ConnectivityResult.none){
        _isOnline = false;
      }else{
        _isOnline = true;
        notifyListeners();
      }
    } catch (e) {
      print("error ${e.toString()}");
    }
  }

  Future<bool> _updateConncetionStatus() async{
    bool isConnceted = false;
    try {
      final List<InternetAddress> result = await InternetAddress.lookup("google.com");
      if(result.isNotEmpty && result[0].rawAddress.isNotEmpty){
        isConnceted = true;
      }
    } on SocketException catch (_) {
      isConnceted = false;
    }
    return isConnceted;
  }

}