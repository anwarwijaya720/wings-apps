import 'package:app/modul/dashboard_screen.dart';
import 'package:app/modul/home/screen/home.dart';
import 'package:app/modul/wings_app.dart';
import 'package:app/service/flavor.dart';
import 'package:flutter/material.dart';

void main() {
  FlavorConfig.flavor = Flavor.PRODUCTION;
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Wings App',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const DashboardScreen(),
    );
  }
}